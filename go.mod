module github.com/aryuuu/cepex-server

go 1.15

require (
	github.com/aws/aws-sdk-go v1.38.7 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/googollee/go-socket.io v1.4.4 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	go.mongodb.org/mongo-driver v1.5.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
